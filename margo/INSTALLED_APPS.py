INSTALLED_APPS = [
    'filebrowser',
    'modeltranslation',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.redirects',
    'django.contrib.flatpages',
    'django.contrib.sitemaps',


    "crispy_forms",
    "tinymce",
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'import_export',
    'rosetta',
    'privat24',


    'core.apps.CoreConfig',
    'pages.apps.PageConfig',
    'content.apps.ContentConfig',
    'order.apps.OrderConfig',
]
