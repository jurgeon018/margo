from django.contrib import admin 
from shop.order.models import * 
from core.admin import custom_admin



class OrderInline(admin.TabularInline):
    model = Order 
    extra = 0
    fields = [
        'name',
        'email',
        'phone',
        'address',
        'created',
    ]
    readonly_fields = [
        'created'
    ]
    def has_change_permission(self, request, obj):
        return False
    def has_delete_permission(self, request, obj):
        return False
    def has_add_permission(self, request, obj):
        return False


class OrderedItemInline(admin.TabularInline):
    def has_delete_permission(self, request, obj=None):
        return False
    def has_add_permission(self, request, obj=None):
        return False
    def has_change_permission(self, request, obj=None):
        print(request.user.is_superuser)
        return False
    model = OrderedItem
    extra = 0
    exclude = [
        'sk',
        'user', 
        'ordered'
    ]
    readonly_fields = [
        "item",
        # "item__size__name",
        # "item__layers",
        # "item__amount",
        "quantity",
        "price_per_item",
        "total_price",
    ]
    can_delete = False


class PaymentInline(admin.StackedInline):
    def has_delete_permission(self, request, obj=None):
        return False
    def has_add_permission(self, request, obj=None):
        return False
    def has_change_permission(self, request, obj=None):
        return False
    model = Payment
    extra = 0
    exclude = [
        'sk',
        'user',
        'ordered'
    ]


@admin.register(Status, site=custom_admin)
class StatusAdmin(admin.ModelAdmin):
    pass


@admin.register(OrderedItem, site=custom_admin)
class OrderedItemAdmin (admin.ModelAdmin):
    list_display = [
        "item",
        "quantity",
        "price_per_item",
        "total_price",
    ]
    exclude = [
        'sk', 'user']
    class Meta:
        model = OrderedItem


@admin.register(Order, site=custom_admin)
class OrderAdmin(admin.ModelAdmin):
    class Meta:
        model = Order
    inlines = [
        OrderedItemInline,
        PaymentInline,
    ]
    list_display = [
        'id',
        'name',
        'email',
        'phone',
        'address',
        'comments',
        'status',
        'payment_opt',
        'delivery_opt'
    ]
    list_editable = [
        'status'
    ]
    exclude = [
        'sk', 
        'user',
    ]
    search_fields = [
        'user__username',
        'name',
        'email',
        'phone',
        'address',
        'comments',
    ]
    list_filter = [
        'status',
        'created'
    ]
    readonly_fields = [
        'name',
        'email',
        'phone',
        'address',
        'comments',
        'total_price',
        'delivery_opt',
        'payment_opt',
        'ordered',
    ]


@admin.register(Payment, site=custom_admin)
class PaymentAdmin(admin.ModelAdmin):
    list_display = [
    'status',
    'amount',
    'status',
    'ip',
    'order',
    'sender_phone',
    'sender_first_name',
    'sender_last_name',
    'sender_card_mask2',
    'sender_card_bank',
    'sender_card_type',
    'sender_card_country',
    ]
    exclude = [
        'sk', 
        'user'
    ]
    class Meta:
        model = Payment

