from django.shortcuts import render 
from django.conf import settings
from django.shortcuts import render, redirect
from core.utils import *



def create_order_items(request, order):
  sk = get_sk(request)
  total_price = 0
  for cart_item in CartItem.objects.filter(ordered=False, sk=sk):
    total_price += cart_item.total_price
  order.total_price = total_price

  cart_items = CartItem.objects.filter(sk=sk)
  if request.user.is_authenticated:
    order.user = request.user
    cart_items.update(user=request.user)

  order.save()
  cart_items.update(order=order)

  for cart_item in CartItem.objects.filter(sk=sk, ordered=False):
    order_item = OrderedItem()
    order_item.item = cart_item.item 
    order_item.user = cart_item.user 
    order_item.sk = cart_item.sk 
    order_item.order = cart_item.order 
    # order_item.size = cart_item.size 
    order_item.quantity = cart_item.quantity 
    order_item.price_per_item = cart_item.price_per_item 
    order_item.total_price = cart_item.total_price 
    order_item.save()
  cart_items.update(ordered=True)
  # cart_items.delete()


def order_items(request):
  flag = 'items'
  if request.method == 'GET':
    cart_items = CartItem.objects.filter(sk=get_sk(request), ordered=False)
    print(cart_items)
    if not cart_items.exists():
      referer = request.META.get('HTTP_REFERER', '/')
      return redirect(referer)
    return render(request, 'checkout.html', locals())
  sk            = get_sk(request)
  name          = request.POST.get('name', '')
  tel           = request.POST.get('tel', '')
  email         = request.POST.get('email', '')
  comments      = request.POST.get('comment', '')
  address       = request.POST.get('address', '')
  payment_opt   = request.POST.get('payment', '')
  delivery_opt  = request.POST.get('delivery', '')
  order         = Order()
  order.name    = name
  order.email   = email
  order.address = address
  order.tel     = tel
  order.comments= comments 
  order.payment_opt = payment_opt
  order.delivery_opt = delivery_opt
  order.sk      = sk
  order.save()

  if payment_opt == 'privat':
    return redirect("pay")
  elif payment_opt == 'manager':
    send_order_mail()
    order.ordered=True
    create_order_items(request, order)
    order.save()
    return redirect('thank_you')

