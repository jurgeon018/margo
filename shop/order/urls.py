from django.urls import path 
from shop.order.views  import *


urlpatterns = [
    path("create_order_items/", create_order_items, name="create_order_items"),
    path("order_items/", order_items, name="order_items"),
]
