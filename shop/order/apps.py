from django import apps 


class OrderConfig(apps.AppConfig):
    name = 'shop.order'
    verbose_name = 'Заказ'
    