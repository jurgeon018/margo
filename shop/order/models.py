from django.db import models 
from django.contrib.auth import get_user_model 
from django.utils.translation import ugettext_lazy as _


User = get_user_model()


class OrderItemManager(models.Manager):
  def all(self):
    return super(OrderItemManager, self).get_queryset().filter(ordered=False)


class Status(models.Model):
  name = models.CharField(max_length=24, blank=True, null=True, default=None)
  is_active = models.BooleanField(default=True)
  created = models.DateTimeField(auto_now_add=True, auto_now=False)
  updated = models.DateTimeField(auto_now_add=False, auto_now=True)

  def __str__(self):
    return f"{self.name}"

  class Meta: 
    verbose_name = ('Статус')
    verbose_name_plural = ('Статуси')


class OrderedItem(models.Model):
  user           = models.ForeignKey(User, verbose_name=_('Пользователь'),on_delete=models.CASCADE, blank=True, null=True)
  sk             = models.CharField(max_length=120, verbose_name=_('Ключ сессии'),blank=True, null=True)
  item           = models.ForeignKey('shop.Item', related_name="ordered_items", verbose_name=_('Товар'),on_delete=models.CASCADE, blank=True, null=True)
  order          = models.ForeignKey('order.Order', related_name='ordered_items', verbose_name=_('Заказ'),on_delete=models.CASCADE, blank=True, null=True)
  # size           = models.CharField(verbose_name=_('Размер'),max_length=20, blank=True, null=True)
  quantity       = models.IntegerField(verbose_name=_('Количество'),default=1)
  price_per_item = models.DecimalField(verbose_name=_('Цена за шт.'),max_digits=10, decimal_places=2, blank=True, null=True)
  total_price    = models.DecimalField(verbose_name=_('Суммарная стоимость товара'),max_digits=10, decimal_places=2, blank=True, null=True)
  ordered        = models.BooleanField(verbose_name=_('Заказан ли?'),default=False, blank=True, null=True)
  created        = models.DateTimeField(verbose_name=_('Дата создания'),auto_now_add=True, auto_now=False, blank=True, null=True)
  updated        = models.DateTimeField(verbose_name=_('Дата обновления'),auto_now_add=False, auto_now=True, blank=True, null=True)
  # objects        = OrderItemManager()

  def __str__(self):
    return f'{self.item.name}, {self.quantity}штука, {self.total_price}'

  def save(self, *args, **kwargs):
    price_per_item = self.item.price
    self.price_per_item = price_per_item
    self.total_price = self.quantity * price_per_item
    super().save(*args, **kwargs)

  class Meta: 
    verbose_name = ('Товар в заказі')
    verbose_name_plural = ('Товари в заказі')


class Order(models.Model):
  user        = models.ForeignKey(User, verbose_name=_('Пользователь'),on_delete=models.CASCADE, blank=True, null=True)
  sk          = models.CharField(verbose_name=_('Ключ сессии'),max_length=120)
  total_price = models.DecimalField(verbose_name=_('Сумма заказа'),max_digits=10, decimal_places=2, default=0)
  name        = models.CharField(verbose_name=_('Имя'),max_length=120, blank=True, null=True)
  email       = models.CharField(max_length=120, blank=True, null=True)
  phone       = models.CharField(verbose_name=_('Номер телефона'),max_length=120, blank=True, null=True)
  address     = models.CharField(verbose_name=_('Адрес'),max_length=120, blank=True, null=True)
  comments    = models.TextField(verbose_name=_('Коментарии'),blank=True, null=True, default=None)
  payment_opt = models.CharField(verbose_name=_("Спосіб оплати"), blank=True, null=True, max_length=120, help_text='manager - без предоплати. \n privat - з предоплатою')
  delivery_opt= models.CharField(verbose_name=_("Спосіб доставки"), blank=True, null=True, max_length=120)
  ordered     = models.BooleanField(verbose_name=_('Заказ завершений'), default=False)
  status      = models.ForeignKey(verbose_name=_('Статус'), max_length=120, on_delete=models.CASCADE, to="Status", blank=True, null=True) 
  created     = models.DateTimeField(verbose_name=_('Дата создания'),auto_now_add=True, auto_now=False, blank=True, null=True)
  updated     = models.DateTimeField(verbose_name=_('Дата обновления'),auto_now_add=False, auto_now=True, blank=True, null=True)

  class Meta: 
    verbose_name = ('Заказ товарів'); 
    verbose_name_plural = ('Закази товаров'); 

  def __str__(self):
    return f'{self.phone}|{self.name}|{self.email}|{self.address}' 


class Payment(models.Model):
  status              = models.CharField(max_length=120, blank=True, null=True)
  ip                  = models.CharField(max_length=120, blank=True, null=True)
  amount              = models.CharField(max_length=120, blank=True, null=True)
  currency            = models.CharField(max_length=120, blank=True, null=True)
  order               = models.OneToOneField('Order', verbose_name='Заказ',on_delete=models.CASCADE, blank=True, null=True)
  sender_phone        = models.CharField(max_length=120, blank=True, null=True)
  sender_first_name   = models.CharField(max_length=120, blank=True, null=True)
  sender_last_name    = models.CharField(max_length=120, blank=True, null=True)
  sender_card_mask2   = models.CharField(max_length=120, blank=True, null=True)
  sender_card_bank    = models.CharField(max_length=120, blank=True, null=True)
  sender_card_type    = models.CharField(max_length=120, blank=True, null=True)
  sender_card_country = models.CharField(max_length=120, blank=True, null=True)
  timestamp           = models.DateTimeField(verbose_name='Время',auto_now_add=True, blank=True, null=True)

  def __str__(self):
    return f'{self.order}|{self.amount}|{self.currency}'

  class Meta: 
    verbose_name = 'Оплата'
    verbose_name_plural = 'Оплати' 

