from django.urls import path 
from . import * 




urlpatterns = [
  path('get_items/<slug>/',       get_items,               name='get_items'),
  path('get_items/',              get_items,               name='get_items'),
  path('is_favour/',              is_favour,               name="is_favour"),
]
