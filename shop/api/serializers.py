from rest_framework import serializers
from shop.models import *


class ItemImageSerializer(serializers.ModelSerializer):
  class Meta:
    model = ItemImage
    exclude = []


class ItemSerializer(serializers.ModelSerializer):
  image = ItemImageSerializer(many=True, read_only=True)
  class Meta:
    model = Item
    exclude = []