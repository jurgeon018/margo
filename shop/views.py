from django.shortcuts import render
from core.utils import get_user, get_sk
from shop.models import * 
from django.db.models import Q


def category(request, slug):
  category = Category.objects.get(slug=slug)
  return render(request, 'category.html', locals())


def item_detail(request, pk):
  item = get_object_or_404(Item, pk=pk)
  return render(request, 'product_card.html', locals())


def shop(request):
  return render(request, 'shop.html', locals())


def search(request):
  items = Item.objects.all()
  query = request.GET.get('q').lower()
  if query:
    items = items.filter(
      Q(name__icontains=query) |
      Q(description__icontains=query)
    ).distinct()
  page = IndexPage.objects.all().first()
  return render(request, 'search_results.html', locals())




