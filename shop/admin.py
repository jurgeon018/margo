from django.contrib import admin 
from import_export.admin import ImportExportModelAdmin
from modeltranslation.admin import TranslationAdmin
from django.conf import settings
from django.shortcuts import reverse 
from shop.models import * 
from shop.cart.models import * 
from core.admin import custom_admin


CURRENT_DOMEN = settings.CURRENT_DOMEN


class ItemImageInline(admin.StackedInline):
    model = ItemImage
    extra = 0


class ItemInline(admin.StackedInline):
    model = Item 
    extra = 0

    
class CategoryInline(admin.StackedInline):
    model = Category 
    extra = 0


class ItemFeatureInline(admin.StackedInline):
    model = ItemFeature
    extra = 0


@admin.register(Category, site=custom_admin)
class CategoryAdmin(admin.ModelAdmin):
    inlines = [
        CategoryInline,
    ]


@admin.register(Size, site=custom_admin)
class SizeAdmin(admin.ModelAdmin):
    pass


@admin.register(ItemImage, site=custom_admin)
class ItemImageAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'item',
        'image',
        'alt',
    ]
    list_display_links = [
        'id'
    ]
    list_editable = [
        'item',
        'image',
        'alt',
    ]
    class Meta:
        model = ItemImage


@admin.register(Item, site=custom_admin)
class ItemAdmin(ImportExportModelAdmin):
    save_on_top = True 
    save_on_bottom = True 
    search_fields = [
        'name',
        'articul',
        # 'category__name',
        'description',
        "old_price",
        "price",
    ]
    list_filter = [
        "available", 
        "is_new", 
        "is_active", 
        "category",
    ]
    list_display = [
        'id',
        'name',
        'price',
        'old_price',
        'available',
        'is_new',
        'is_active',
    ]
    list_editable = [
    #     'price',
    #     'old_price',
    #     'available',
    #     'is_new',
    #     'is_active',
    ]
    list_display_links = [
        'id', 
        'name',
    ]
    inlines = [
        ItemImageInline,
        ItemFeatureInline,
    ]


@admin.register(ItemFeature, site=custom_admin)
class ItemFeature(admin.ModelAdmin):
    pass


@admin.register(FeatureCategory, site=custom_admin)
class FeatureCategory(admin.ModelAdmin):
    pass



