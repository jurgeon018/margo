from modeltranslation.translator import translator, TranslationOptions, register
from .models import *



@register(Item)
class ItemTranslationOptions(TranslationOptions):
    fields = (
        'meta_title',
        'meta_descr',
        'name',
        'description',
        'slug',
    )

@register(Category)
class CategoryTranslationOptions(TranslationOptions):
    fields = (
        'title',
        'description',
        'slug',
    )

@register(Size)
class SizeTranslationOptions(TranslationOptions):
    fields = (
        'name',
    )



