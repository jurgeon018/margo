from django.urls import path 
from shop.views import * 



urlpatterns = [
  path("shop/",               shop,        name='shop'),
  path("category/<slug>/",    category,    name='category'),
  path("item/<pk>/",          item_detail, name='item_detail'),
  path("search/",             search,      name='search'),
]
