from django.db.models.signals import post_save, pre_save
from transliterate import translit
from django.utils.text import slugify
from shop.models import Item


def post_save_item_slug(sender, instance, *args, **kwargs):
  if not instance.slug:
    print('not')
    try:
      slug = slugify(translit(instance.name, reversed=True))
    except:
      slug = slugify(instance.name)
    instance.slug = slug + str(instance.id)
    instance.save()


