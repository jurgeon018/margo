from django.db import models
from django.shortcuts import reverse
from PIL import Image
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model


User = get_user_model()


class Category(models.Model):
  meta_title  = models.CharField(max_length=120, blank=True, null=True)
  meta_descr  = models.TextField(blank=True, null=True)
  parent      = models.ForeignKey(verbose_name="Батьківська категорія", to='self', blank=True, null=True, on_delete=models.CASCADE, related_name='categories')
  code        = models.CharField(verbose_name=("Код"), max_length=20, blank=True, null=True)
  title       = models.CharField(verbose_name=("Назва"), max_length=120, blank=True, null=True)
  description = models.TextField(verbose_name=("Опис"), blank=True, null=True)
  slug        = models.SlugField(verbose_name=("Посилання"), max_length=255, blank=True, null=True)
  is_active   = models.BooleanField(verbose_name="Активна", default=True)
  image       = models.ImageField(verbose_name="Зображення", upload_to='cat_pics', blank=True, null=True)
  created     = models.DateTimeField(verbose_name="Створено", auto_now_add=True, auto_now=False, blank=True, null=True)
  updated     = models.DateTimeField(verbose_name="Оновлено", auto_now_add=False, auto_now=True, blank=True, null=True)

  def __str__(self): 
    return self.title

  class Meta: 
    verbose_name = 'Категорія товарів'; 
    verbose_name_plural = 'Категорії товарів'; 
    
  def get_absolute_url(self):
      return reverse("category_detail", kwargs={"slug": self.slug})
  

class Size(models.Model):
  code = models.CharField(max_length=120, blank=True, null=True)
  name = models.CharField(max_length=120, blank=True, null=True)

  class Meta: 
    verbose_name=("Розмір")
    verbose_name_plural=("Розміри"); 

  def __str__(self): 
    return f'{self.name}'


class ItemManager(models.Manager):
  def all(self):
    return super(ItemManager, self).get_queryset().filter(is_active=True)


class ItemImage(models.Model):
  item  = models.ForeignKey(to="shop.Item", on_delete=models.CASCADE, related_name='images')
  image = models.ImageField(verbose_name=('Ссилка зображення'), upload_to='item_pics', blank=True, null=True)
  alt   = models.CharField(max_length=120, blank=True, null=True)

  def __str__(self):
      return "%s" % self.image.url
      
  def save(self, *args, **kwargs):
    super().save(*args, **kwargs)
    img = Image.open(self.image.path)
    img = img.resize((400, 400), Image.ANTIALIAS)
    img.save(self.image.path)

  class Meta: 
    verbose_name = ('Зображення товарів'); 
    verbose_name_plural = ('Зображення товарів'); 


class Item(models.Model):
  meta_title  = models.TextField(verbose_name="Meta title", blank=True, null=True)
  meta_descr  = models.TextField(verbose_name="Meta description", blank=True, null=True)
  name        = models.CharField(verbose_name=("Назва"), max_length=120, blank=True, null=True)
  description = models.TextField(verbose_name=("Опис"), blank=True, null=True)
  articul     = models.CharField(verbose_name=("Артикул"), max_length=20, blank=True, null=True)   
  category    = models.ForeignKey(verbose_name=("Категорія"), to='Category', on_delete=models.CASCADE, blank=True, null=True, help_text=' ')    
  slug        = models.SlugField(verbose_name=("Ссилка"), max_length=255, blank=True, null=True, unique=True)
  old_price   = models.DecimalField(verbose_name=("Стара ціна"), max_digits=10, decimal_places=2, default=0, blank=True, null=True)
  price       = models.DecimalField(verbose_name=("Нова ціна"), max_digits=10, decimal_places=2, default=0, blank=True, null=True)
  available   = models.BooleanField(verbose_name=("Є в наявності"), default=True)
  is_new      = models.BooleanField(verbose_name=("Новий"), default=False, help_text="Мітка New на товарі на сайті")
  is_active   = models.BooleanField(verbose_name=("Активний"), default=True, help_text="Відображення товару на сайті в списку товарів")
  created     = models.DateTimeField(verbose_name=("Створений"), auto_now_add=True,  auto_now=False, blank=True, null=True)
  updated     = models.DateTimeField(verbose_name=("Оновлений"), auto_now_add=False, auto_now=True,  blank=True, null=True)
  objects     = ItemManager()

  class Meta: 
    verbose_name = ('Товар'); 
    verbose_name_plural = ('Товари')

  def __str__(self):
    return f"{self.name}"

  def get_absolute_url(self):
      return reverse("item_detail", kwargs={"slug": self.slug})
  

class ItemFeature(models.Model):
  item  = models.ForeignKey(verbose_name="Товар", to="shop.Item", related_name="features", on_delete=models.CASCADE)
  code  = models.CharField(verbose_name="Код", max_length=120, unique=True)
  name  = models.CharField(verbose_name="Назва характеристики", max_length=120)
  value = models.TextField(verbose_name="Значення характеристики")
  category = models.ForeignKey(verbose_name="Категорія", to="FeatureCategory", related_name="items", on_delete=models.CASCADE)

  def __str__(self):
    return f"{self.item}, {self.code}, {self.name}"

  class Meta:
    verbose_name = 'Характеристика товарів'
    verbose_name_plural = 'Характеристики товарів'


class FeatureCategory(models.Model):
  name = models.CharField(max_length=120, unique=True)
  
  def __str__(self):
    return f"{self.name}"

  class Meta:
    verbose_name = 'Категорія характеристик'
    verbose_name_plural = 'Категорії характеристик'


