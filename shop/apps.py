from django.apps import AppConfig

class ShopConfig(AppConfig):
    name = 'shop'
    verbose_name = 'магазин'
    def ready(self):
        from shop.models import Item 
        from shop.signals import post_save_item_slug, post_save
        post_save.connect(post_save_item_slug, sender=Item)

