from django.contrib import admin 
from core.admin import custom_admin
from shop.cart.models import *


class CartItemInline(admin.TabularInline):
    model = CartItem
    extra = 0
    # exclude = ['sk']
    readonly_fields = ['sk','user', 'item', 'quantity',]


class CartItemInline(admin.TabularInline):
    model = CartItem
    extra = 0
    # exclude = ['sk']
    readonly_fields = ['sk','user', 'item', 'quantity',]

@admin.register(CartItem, site=custom_admin)
class CartItemAdmin(admin.ModelAdmin):
    list_display = [field.name for field in CartItem._meta.fields]
    exclude = ['sk', 'user']
    class Meta:
        model = CartItem

@admin.register(FavourItem, site=custom_admin)
class FavourItemAdmin(admin.ModelAdmin):
    list_display = [field.name for field in FavourItem._meta.fields]
    exclude = ['sk', 'user']
    class Meta:
        model = FavourItem


@admin.register(Cart, site=custom_admin)
class CartAdmin(admin.ModelAdmin):
    pass