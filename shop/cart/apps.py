from django import apps 


class CartConfig(apps.AppConfig):
    name = 'shop.cart'
    verbose_name = "корзина"
