from rest_framework import serializers
from shop.models import *
from shop.cart.models import *
from shop.order.models import *
from shop.api.serializers import *


class CartItemSerializer(serializers.ModelSerializer):
  item = ItemSerializer()
  class Meta:
    model = CartItem
    exclude = []


class FavourItemSerializer(serializers.ModelSerializer):
  item = ItemSerializer()
  class Meta:
    model = FavourItem
    exclude = []
