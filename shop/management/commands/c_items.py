from django.core.management.base import BaseCommand
from core.models import (
  Item, ItemImage, Category, Size
)
import random
import datetime 
import json 
import csv


class Command(BaseCommand):
  def add_arguments(self, parser):
    parser.add_argument(
      'file_name', 
      type=str, 
      help='File, that contains the main item\'s information'
    )
  def handle(self, *args, **kwargs):
    file = open(f"{kwargs['file_name']}")
    products = csv.reader(file)
    for product in list(products)[1:]:
      name     = product[0]
      name_ru  = product[1]
      name_en  = product[2]
      name_uk  = product[3]
      category = product[4]
      size     = product[5]
      amount   = product[6]
      layers   = product[7]
      url      = 'item_pics/everyday/'+product[8]
      price    = product[9]
      descr    = product[10]
      descr_ru = product[11]
      descr_en = product[12]
      descr_uk = product[13]
      
      cat, _   = Category.objects.get_or_create(code=category)
      size, _  = Size.objects.get_or_create(code=size)
      item, _  = Item.objects.get_or_create(
        name=name,
        name_ru=name_ru,
        name_en=name_en,
        name_uk=name_uk,
      )
      image, _ = ItemImage.objects.get_or_create(image=url, item=item)
      item.size  = size
      item.price = price
      item.old_price = 0
      try:
        item.category = cat
      except:
        item.category.add(cat)
      item.articul  = str(item.pk).zfill((3))
      item.layers   = layers
      item.amount   = amount
      item.description    = descr
      item.description_ru = descr_ru
      item.description_en = descr_en
      item.description_uk = descr_uk
      item.save()
      print(item, item.pk)
    self.stdout.write(self.style.SUCCESS('Data imported successfully'))
