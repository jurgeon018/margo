from core.admin import custom_admin
from .models import * 
from django import forms 
from django.contrib.auth.forms import (
    ReadOnlyPasswordHashWidget, ReadOnlyPasswordHashField,
    UsernameField, UserCreationForm,
    UserChangeForm, AuthenticationForm,
    PasswordResetForm, SetPasswordForm,
    AdminPasswordChangeForm,
)
from django.contrib import admin 
from shop.order.admin import OrderInline


class UserChangeForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField()
    class Meta:
        model = User
        # fields = ('email', 'password',  'is_active')
        exclude = []
    def clean_password(self):
        return self.initial["password"]


class CustomUserCreationForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('username', 'email')


class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = User
        fields = ('username', 'email')


class UserAdmin(admin.ModelAdmin):
    form = UserChangeForm
    add_form = UserCreationForm
    change_password_form = AdminPasswordChangeForm
    list_per_page = 10
    list_display = (
        'id', 
        'username',
        'email',
        'first_name',
        'last_name',
        'phone_number',
    )
    list_display_links = [
        'id',
        'email',
        'username',
        'first_name',
        'last_name',
        'phone_number',
    ]
    search_fields = [
        'email',
        'username',
        'first_name',
        'last_name',
        'phone_number',
    ]

    inlines = [
        OrderInline
    ]
    fields = [
        'username',
        'email',
        'first_name',
        'last_name',
        'phone_number',
    ]
    readonly_fields = [
        'username',
        'email',
        'first_name',
        'last_name',
        'phone_number',
    ]



# custom_admin.register(UserAdmin)

