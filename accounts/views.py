from django.shortcuts import render, redirect, reverse 




@login_required
def profile(request):
    orders = Order.objects.filter(
      user=get_user(request)
    ).order_by('-id')
    if request.method == 'POST':# and u_form.is_valid() and p_form.is_valid():
        first_name   = request.POST['first_name']
        last_name    = request.POST['last_name']
        phone_number = request.POST['phone_number']
        email = request.POST['email']
        user = request.user
        user.first_name=first_name
        user.last_name = last_name
        user.phone_number = phone_number
        user.email = email
        user.save()
        messages.success(request, 'Ваши данные были обновлены')
        return redirect(reverse('profile'))
    return render(request, 'profile.html', locals())




@login_required
def delete_order(request, pk):
  order = get_object_or_404(Order, pk=pk, user=get_user(request))
  order.delete()
  messages.success(request, _('order was deleted'))
  return redirect('profile')
