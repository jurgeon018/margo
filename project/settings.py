import os
from decouple import config
from django.utils.translation import ugettext_lazy as _


INSTALLED_APPS = [
    'filebrowser',
    'modeltranslation',

    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.redirects',
    'django.contrib.flatpages',
    'django.contrib.sitemaps',

    'accounts.apps.AuthConfig',
    'core.apps.CoreConfig',
    'shop.apps.ShopConfig',
    'shop.order.apps.OrderConfig',
    'shop.cart.apps.CartConfig',
    'pages.apps.PageConfig',
    'liqpay.apps.LiqpayConfig',
    'content.apps.ContentConfig',
    'blog.apps.BlogConfig',

    'privat24',
    "crispy_forms",
    "tinymce",
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'import_export',
    'rosetta',
]

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
CURRENT_DOMEN = config('CURRENT_DOMEN', 'margoltd.com') 
SECRET_KEY = config('SECRET_KEY') or 'ss'
DEBUG      = config('DEBUG') or True
ADMINS = (('Alexey', 'jurgeon018@gmail.com'), ('Andrew', 'jurgeon019@gmail.com'))
ROOT_URLCONF = 'core.urls'
WSGI_APPLICATION = 'project.wsgi.application'
TIME_ZONE = 'UTC' #'Europe/Kiev'
USE_L10N = True
USE_I18N = True
USE_TZ = True
LANGUAGE_CODE = 'ru'
LANGUAGES = (
  ('ru', _('Russian')),
  ('en', _('English')),
  ('uk', _('Ukrainian')),
)
LOCALE_PATHS = (os.path.join(BASE_DIR, 'locale'),)
STATIC_URL = '/static/'
STATICFILES_DIRS = (os.path.join(BASE_DIR, "static"),)
STATIC_ROOT = os.path.join(BASE_DIR, "static_root")
MEDIA_ROOT = os.path.join(BASE_DIR, "media")
MEDIA_URL = '/media/'
LOGIN_REDIRECT_URL = 'profile'
CRISPY_TEMPLATE_PACK = 'bootstrap4'
SITE_ID=1
ROSETTA_MESSAGES_PER_PAGE = 100
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend'
)
ACCOUNT_LOGOUT_ON_GET = True
AUTH_USER_MODEL = 'accounts.User'

import os
from decouple import config


from configparser import RawConfigParser
# config = RawConfigParser()
# config.read('/home/jurgeon/dev/margo/settings.ini')
# config.read('../../settings.ini')
#
# EMAIL_HOST_USER     = config.get('section', 'jurgeon_EMAIL_HOST_USER')
# EMAIL_HOST_PASSWORD = config.get('section', 'jurgeon_EMAIL_HOST_PASSWORD')
EMAIL_HOST          = 'smtp.gmail.com'
EMAIL_PORT          = 587
EMAIL_USE_TLS       = True
# EMAIL_HOST_USER     = config.get('section', 'margo_EMAIL_HOST_USER')
# EMAIL_HOST_PASSWORD = config.get('section', 'margo_EMAIL_HOST_PASSWORD')
# EMAIL_HOST          = 'mail.adm.tools'
# EMAIL_PORT          = 25
# EMAIL_USE_TLS       = False
# EMAIL_USE_SSL       = False


# print(EMAIL_HOST_PASSWORD, EMAIL_HOST_USER)



DEFAULT_FROM_EMAIL = 'Margo Site Team <info@margoltd.com>'
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'






AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator'},
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator'},
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator'},
    {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator'}
]


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',

                'shop.cart.context_processors.cart_content',
            ],
        },
    },
]


MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
    'django.contrib.redirects.middleware.RedirectFallbackMiddleware',
]

try:
    from django.contrib.redirects.middleware import RedirectFallbackMiddleware
except:
    pass




# DEBUG = False   #python manage.py runserver --insecure # for 404 page
DEBUG = True   

ALLOWED_HOSTS = ['*']
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
#       'ENGINE': 'django.db.backends.postgresql_psycopg2',
#       'NAME': 'margo_db',
#       'USER' : 'jurgeon018',
#       'PASSWORD' : '69018',
#       'HOST' : '127.0.0.1',
#       'PORT' : '5432',
  }
}

LIQPAY_PUBLIC_KEY = "sandbox_i29048427621"
LIQPAY_PRIVATE_KEY = "sandbox_RBR5FM04gXXt25MLzVmP7eyarKDWIKXw86MEMkvm"

# LIQPAY_PUBLIC_KEY = "i3466565002"
# LIQPAY_PRIVATE_KEY="85pd0UjyxXThv8RQpmPld4Z406wGZF4huAfqDHaB"


