from django.contrib import admin 
from core.admin import custom_admin
from blog.models import *


class CommentInline(admin.StackedInline):
    model = Comment
    extra = 0


class PostInline(admin.StackedInline):
    model = Post
    extra = 0



@admin.register(Post, site=custom_admin)
class PostAdmin(admin.ModelAdmin):
    inlines = [
        CommentInline,
    ]


@admin.register(Comment, site=custom_admin)
class CommentAdmin(admin.ModelAdmin):
    inlines = [
        CommentInline,
    ]

