from django import apps 

class BlogConfig(apps.AppConfig):
    name = 'blog'
    verbose_name = 'блог'
    