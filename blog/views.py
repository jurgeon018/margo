from django.shortcuts import render, redirect, reverse 



def blog(request):
  posts = Post.objects.all()
  return render(request, 'blog.html', locals())


def post_detail(request, pk):
  post = Post.objects.get(pk=pk)
  return render(request, 'post_detail.html', locals())

