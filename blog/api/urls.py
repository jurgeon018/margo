from django.urls import path 
from blog.api.views import *


urlpatterns = [
    path('get_posts/', get_posts, name="get_posts"),
]
