from django.http import HttpResponse, JsonResponse
from blog.models import *
from blog.api.serializers import * 


def get_posts(request):
    posts = Post.objects.all()
    response = PostSerializer(posts, many=True)
    return JsonResponse(response)