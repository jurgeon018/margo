from django.urls import path 
from django.conf.urls.i18n import i18n_patterns
from blog.views import * 



urlpatterns += i18n_patterns(
  path("blog/",               blog,        name='blog'),
  path("post_detail/<slug>/", post_detail, name='post_detail'),
]
