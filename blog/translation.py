from modeltranslation.translator import (
    translator, TranslationOptions, register
)
from blog.models import *


@register(Post)
class PostTranslationOptions(TranslationOptions):
    fields = (
        "meta_title",
        "meta_description",
        "title",
        "content",
        "slug",  
    )




