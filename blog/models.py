from django.db import models 
from tinymce.models import HTMLField
from django.contrib.auth import get_user_model


User = get_user_model() 


class Post(models.Model):
  meta_title       = models.CharField(verbose_name="Мета Заголовок", max_length=120, blank=True, null=True)
  meta_description = models.TextField(verbose_name="Мета Опис", blank=True, null=True)
  title            = models.CharField(verbose_name=("Заголовок"),max_length=120, blank=True, null=True)
  content          = HTMLField(verbose_name=("Контент"), blank=True, null=True)  
  slug             = models.SlugField(verbose_name="Ссилка", blank=False, null=False, max_length=255, unique=True)
  image            = models.ImageField(verbose_name="Картинка", blank=True, null=True)
  author           = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
  updated          = models.DateTimeField(verbose_name="Оновлено", auto_now_add=False, auto_now=True)
  created          = models.DateTimeField(verbose_name="Створено", auto_now_add=True, auto_now=False)

  def __str__(self):
    return f'{self.title}'

  class Meta:
    verbose_name = 'Публікація'
    verbose_name_plural = 'Публікації'

  def get_absolute_url(self):
      return reverse("post_detail", kwargs={"slug": self.slug})


class Comment(models.Model):
  parent  = models.ForeignKey('self', on_delete=models.CASCADE, blank=True, null=True, related_name='comments')
  post    = models.ForeignKey(to="blog.Post", blank=True, null=True, related_name='comments', on_delete=models.CASCADE)
  title   = models.CharField(verbose_name=("Заголовок"),max_length=120, blank=True, null=True)
  content = models.TextField(verbose_name=("Коммент"), blank=True, null=True)  
  author  = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
  updated = models.DateTimeField(verbose_name="Оновлено", auto_now_add=False, auto_now=True)
  created = models.DateTimeField(verbose_name="Створено", auto_now_add=True, auto_now=False)

  def __str__(self):
    return f"{self.title}"

  class Meta:
    verbose_name = 'Коментар'
    verbose_name_plural = 'Коментарі'

