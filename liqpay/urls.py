from django.urls import path 
from .views import * 


urlpatterns = [
  path("pay/", pay, name='pay'),
  path("pay_callback/", pay_callback, name='pay_callback'),
]
