from django.urls import path, include

from django.conf import settings

from filebrowser.sites import site 

from django.contrib import admin
from core.admin import custom_admin
from core.views import * 
from core.utils import *

from django.conf.urls.i18n import i18n_patterns
from django.views.i18n import JavaScriptCatalog as js_cat

from django.contrib.sitemaps.views import sitemap 
from django.views.generic import TemplateView as t_v

from shop.sitemaps import * 
from blog.sitemaps import * 
from pages.sitemaps import * 


sitemaps = {
  'items': ItemSitemap,
  'categories': CategorySitemap,
  'static':StaticViewSitemap,
}


urlpatterns = [
<<<<<<< HEAD
  path('tinymce/',    include('tinymce.urls')),
  path('i18n/',       include('django.conf.urls.i18n')),
  path('rosetta/',    include('rosetta.urls')),
  path('jsi18n/',     JavaScriptCatalog.as_view(), name='javascript-catalog'),
  path('robots.txt/', TemplateView.as_view(template_name="robots.txt"), name='robots'),
  path('sitemap.xml/',sitemap, {'sitemaps':sitemaps}),
  path('admin/',      manager_admin_site.urls),
  path('filebrowser/',site.urls),


  path('set_language/<lang>/',    set_language_from_url,   name="set_language_from_url"),

  path('set_logo/',               set_logo,                name='set_logo'),
  path('set_params/',             set_params,              name='set_params'),
  path('remove_logo/',            remove_logo,             name="remove_logo"),

  path('user_contact/',           user_contact,            name='user_contact'),

  path('pay_callback/',           pay_callback,            name='pay_callback'),

  path('delete_order/<pk>/',      delete_order,            name='delete_order'),

  path('get_items/<slug>/',       get_items,               name='get_items'),
  path('get_items/',              get_items,               name='get_items'),
  path('is_favour/',              is_favour,               name="is_favour"),
  path('get_cart_items_amount/',  get_cart_items_amount,   name='get_cart_items_amount'),
  path('get_cart_items/',         get_cart_items,          name='get_cart_items'),
  path('add_cart_item/',          add_cart_item,           name='add_cart_item'),
  path('remove_cart_item/',       remove_cart_item,        name='remove_cart_item'),
  path('change_cart_item_amount/',change_cart_item_amount, name='change_cart_item_amount'),
  path('clear_cart/',             clear_cart,              name='clear_cart'),
  path('get_favours_amount/',     get_favours_amount,      name='get_favours_amount'),
  path('get_favours/',            get_favours,             name='get_favours'),
  path('add_favour/',             add_favour,              name='add_favour'),
  path('remove_favour/',          remove_favour,           name='remove_favour'),
  path('add_favour_to_cart/',     add_favour_to_cart,      name='add_favour_to_cart'),
  path('add_favours_to_cart/',    add_favours_to_cart,     name='add_favours_to_cart'),
  path('add_favour_by_like/',     add_favour_by_like,      name='add_favour_by_like'),
  path('remove_favour_by_like/',  remove_favour_by_like,   name='remove_favour_by_like'),
=======
  path('tinymce/',         include('tinymce.urls')),
  path('i18n/',            include('django.conf.urls.i18n')),
  path('rosetta/',         include('rosetta.urls')),
  path('admin/',           custom_admin.urls),
  path('filebrowser/',     site.urls),
  path('sitemap.xml/',     sitemap, {'sitemaps':sitemaps}),
  path('robots.txt/',      t_v.as_view(template_name="robots.txt"), name='robots'),
  path('set_lang/<lang>/', set_lang, name="set_lang"),
  path('jsi18n/',          js_cat.as_view(), name='javascript-catalog'),
  path('', include('shop.cart.api.urls')),
  path('', include('shop.order.api.urls')),
  path('', include('blog.api.urls')),
>>>>>>> cf4dc8b1376017a23266d2d63dfe19689278094a
]


urlpatterns += i18n_patterns(
<<<<<<< HEAD
  path("",                    index,       name='index'),
  path("horeca/",             horeca,      name='horeca'),
  path("blog/",               blog,        name='blog'),
  path("post_detail/<slug>/", post_detail, name='post_detail'),
  path("privatelabel/",       privatelabel,name='privatelabel'),
  path("constructor/",        constructor, name='constructor'),
  path("partnership/",        partnership, name='partnership'),
  path("contact/",            contact,     name='contact'),
  path("shop/",               shop,        name='shop'),
  path("servets_categories/", servets_cat, name='servets_cat'),
  path("category/<slug>/",    category,    name='category'),
  path("item/<pk>/",          item_detail, name='item_detail'),
  path("order_items/",        order_items, name='order_items'),
  path("order_napkin/",       order_napkin,name='order_napkin'),
  path("pay/",                pay,         name='pay'),
  path("search/",             search,      name='search'),
  path("profile/",            profile,     name='profile'),
  path("thank_you/",          thank_you,   name='thank_you'),
  path('accounts/',           include('allauth.urls')),
=======
  path('accounts/', include('allauth.urls')),
  path('',          include('pages.urls')),
  path('',          include('shop.urls')),
  path('',          include('liqpay.urls')),
  path('',          include('shop.order.urls')),
  path('',          include('pages.urls')),
>>>>>>> cf4dc8b1376017a23266d2d63dfe19689278094a
  prefix_default_language=True,
)


if settings.DEBUG == True:
  from django.conf.urls.static import static
  urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
  urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


