from django.contrib import admin
from django.contrib.redirects.models import Redirect
from django.contrib.sites.models import Site



class CustomAdmin(admin.AdminSite):
    site_header = "Site Admin"
    site_title  = "Site Admin"
    index_title = "Site Admin"

    

custom_admin = CustomAdmin(name='custom_admin')