from django.contrib.sitemaps import Sitemap 
from core.models import *
from django.urls import reverse  


class ItemSitemap(Sitemap):
    changefreq = 'weekly'
    priority = 1
    protocol = 'https'
    i18n = True 
    def items(self):
        return Item.objects.all()
    def lastmod(self, obj):
        return obj.updated

class CategorySitemap(Sitemap):
    changefreq = 'never'
    priority = 1
    protocol = 'https'
    i18n = True
    def items(self):
        return Category.objects.all()
    def lastmod(self, obj):
        return obj.updated

class StaticViewSitemap(Sitemap):
    i18n = True 
    def items(self):
        return [
            'index', 
            'horeca',
            'privatelabel',
            'constructor',
            'partnership',
            'contact',
            'shop',
            'servets_cat',
        ]
    def location(self, item):
        return reverse(item)



