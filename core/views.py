

def handler_404(request, exception):
  return render(request,'404.html', locals())


def handler_500(request):
  return render(request,'500.html', locals())


handler404 = 'core.views.handler_404'
handler500 = 'core.views.handler_500'

