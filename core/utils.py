import random 
from django.core.exceptions import ObjectDoesNotExist
import string
from django.contrib import messages
from django.conf import settings
from django.core.mail import send_mail


CURRENT_DOMEN = settings.CURRENT_DOMEN


def set_lang(request, lang=None):
  from django.utils import translation
  # lang = request.POST['lang']
  translation.activate(lang)
  request.session[translation.LANGUAGE_SESSION_KEY] = lang
  # return redirect(request.META['HTTP_REFERER'])
  url = request.META['HTTP_REFERER'].split('/')
  url[3] = lang
  url = '/'.join(url)
  return redirect(url)


def get_sk(request):
  sk = request.session.session_key
  if not sk: 
    request.session.cycle_key()
  return sk 


def get_user(request):
  if request.user.is_anonymous:
    return None
  return request.user


def send_contact_mail():
  send_mail(
    subject = 'Contact form Received',
    # message = get_template('contact_message.txt').render({'message':message}),
    message = f'Були отримані контактні дані. Перейдіть по цій ссилці: {CURRENT_DOMEN}/admin/core/usercontact/',
    from_email = settings.DEFAULT_FROM_EMAIL,
    recipient_list = [settings.DEFAULT_FROM_EMAIL],#, email],
    fail_silently=True,
  )


def send_order_mail():
  send_mail(
    subject = 'Order form Received',
    # message = get_template('contact_message.txt').render({'message':message}),
    message = f'Було отримано замовлення товарів. Перейдіть по цій ссилці: {CURRENT_DOMEN}/admin/core/order/',
    from_email = settings.DEFAULT_FROM_EMAIL,
    recipient_list = [settings.DEFAULT_FROM_EMAIL],#, email],
    fail_silently=True,
  )



def send_napkin_mail():
  send_mail(
    subject = 'Order form Received',
    # message = get_template('contact_message.txt').render({'message':message}),
    message = f'Було отримано замовлення салфетки. Перейдіть по цій ссилці: {CURRENT_DOMEN}/admin/core/napkin/',
    from_email = settings.DEFAULT_FROM_EMAIL,
    recipient_list = [settings.DEFAULT_FROM_EMAIL],#, email],
    fail_silently=True,
  )


