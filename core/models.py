from django.conf import settings
from django.contrib.auth.models import User, AbstractBaseUser,AbstractUser
from django.utils.translation import ugettext as _
from django.db import models
from django.utils.text import slugify
from django.db.models import Sum
from django.shortcuts import reverse
from PIL import Image
from django.db.models.signals import post_save, pre_save
from django.contrib.auth import get_user_model

from django.db import models
from django.conf import settings 
from django.db.models.signals import post_save, pre_save
from django.utils.text import slugify
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from PIL import Image
from transliterate import translit


# Профиль

class User(AbstractUser):
  phone_number = models.CharField(_("Phone Number"), max_length=50, default='')

User = get_user_model()


# Продукты

class ItemManager(models.Manager):
  def all(self):
    return super(ItemManager, self).get_queryset().filter(is_active=True)


class Category(models.Model):
  subcat      = models.ForeignKey('self',blank=True, null=True, on_delete=models.CASCADE)
  code        = models.CharField(max_length=20, blank=True, null=True)
  title       = models.CharField(max_length=120, blank=True, null=True)
  description = models.TextField(blank=True, null=True)
  slug        = models.SlugField(max_length=255, blank=True, null=True)
  is_active   = models.BooleanField(default=True)
  image       = models.ImageField(upload_to='cat_pics', blank=True, null=True)
  created     = models.DateTimeField(auto_now_add=True, auto_now=False, blank=True, null=True ,verbose_name="Створено")
  updated     = models.DateTimeField(auto_now_add=False, auto_now=True, blank=True, null=True ,verbose_name="Оновлено")

  def __str__(self): return self.title
  class Meta: verbose_name = 'Категорія'; verbose_name_plural = 'Категорії'; 
  def get_absolute_url(self):
      return reverse("category", kwargs={"slug": self.slug})
  


class Size(models.Model):
  code = models.CharField(max_length=120, blank=True, null=True)
  name = models.CharField(max_length=120, blank=True, null=True)
  class Meta: verbose_name=_("Розмір"); verbose_name_plural=_("Розміри"); 
  def __str__(self): return f'{self.name}'


class ItemImage(models.Model):
  item      = models.ForeignKey('core.Item', verbose_name='Товар',related_name='image',blank=True, null=True, default=None, on_delete=models.CASCADE)
  image     = models.ImageField(verbose_name=_('Ссылка изображения'), upload_to='item_pics', blank=True, null=True)
  alt       = models.CharField(max_length=120, blank=True, null=True)
  def __str__(self):
      return "%s" % self.image.url
  def save(self, *args, **kwargs):
    super().save(*args, **kwargs)
    img = Image.open(self.image.path)
    img = img.resize((400, 400), Image.ANTIALIAS)
    img.save(self.image.path)
  class Meta: verbose_name = _('Изображение товара'); verbose_name_plural = _('Изображения товара'); 


class Item(models.Model):
  name        = models.CharField(verbose_name=_("Назва"), max_length=120, blank=True, null=True)
  articul     = models.CharField(verbose_name=_("Артикул"), max_length=20, blank=True, null=True)   
  size        = models.ForeignKey(Size, verbose_name=_("Розмір"), on_delete=models.CASCADE, blank=True, null=True)
  layers      = models.CharField(verbose_name=_("Шаровість"),max_length=10, blank=True, null=True)
  amount      = models.CharField(verbose_name=_("К-сть в пачці"),max_length=20, blank=True, null=True)
  # category    = models.ForeignKey(Category, verbose_name=_("Категорія"), blank=True, null=True, on_delete=models.CASCADE)    
  category    = models.ManyToManyField('core.Category', verbose_name=_("Категорія"), blank=True, null=True, help_text=' ')    
  description = models.TextField(verbose_name=_("Опис"), blank=True, null=True)
  meta_title  = models.TextField(verbose_name="Meta title", blank=True, null=True)
  meta_descr  = models.TextField(verbose_name="Meta description", blank=True, null=True)
  slug        = models.SlugField(max_length=255, verbose_name=_("Ссилка"), blank=True, null=True, unique=True)
  old_price   = models.DecimalField(verbose_name=_("Стара ціна"), max_digits=10, decimal_places=2, default=0, blank=True, null=True)
  price       = models.DecimalField(verbose_name=_("Нова ціна"), max_digits=10, decimal_places=2, default=0, blank=True, null=True)
  available   = models.BooleanField(verbose_name=_("Є в наявності"), default=True)
  is_new      = models.BooleanField(verbose_name=_("Новий"), default=False, help_text="Мітка New на товарі на сайті")
  is_active   = models.BooleanField(verbose_name=_("Активний"), default=True, help_text="Відображення товару на сайті в списку товарів")
  created     = models.DateTimeField(verbose_name=_("Створений"), auto_now_add=True,  auto_now=False, blank=True, null=True)
  updated     = models.DateTimeField(verbose_name=_("Оновлений"), auto_now_add=False, auto_now=True,  blank=True, null=True)
  objects     = ItemManager()
  class Meta: verbose_name = _('Товар'); verbose_name_plural = _('Товары')
  def __str__(self):
    return f"{self.name}"
  def get_absolute_url(self):
      return reverse("item_detail", kwargs={"pk": self.pk})
  


def post_save_item_slug(sender, instance, *args, **kwargs):
  if not instance.slug:
    print('not')
    try:
      slug = slugify(translit(instance.name, reversed=True))
    except:
      slug = slugify(instance.name)
    instance.slug = slug + str(instance.id)
    instance.save()
post_save.connect(post_save_item_slug, sender=Item)














class FavourItem(models.Model):
  item = models.ForeignKey('core.Item', on_delete=models.CASCADE, verbose_name='Улюблені товари', blank=True, null=True)
  sk   = models.CharField(max_length=120, blank=True, null=True)
  user = models.ForeignKey(User, verbose_name='Пользователь',on_delete=models.CASCADE, blank=True, null=True)
  def __str__(self):
    return f'{self.item.name},{self.user}, {self.sk}'
  class Meta:
    verbose_name=_("Улюблений товар"); verbose_name_plural=_("Улюблені товари"); 



class CartItem(models.Model):
  user           = models.ForeignKey(User, verbose_name='Пользователь',on_delete=models.CASCADE, blank=True, null=True)
  sk             = models.CharField(verbose_name='Ключ сессии',max_length=120, blank=True, null=True)
  item           = models.ForeignKey('core.Item', verbose_name='Товар',on_delete=models.CASCADE, blank=True, null=True)
  order          = models.ForeignKey('core.Order', verbose_name='Заказ',on_delete=models.CASCADE, blank=True, null=True)
  quantity       = models.IntegerField(verbose_name='Количество',default=1)
  price_per_item = models.DecimalField(verbose_name='Цена за шт',max_digits=10, decimal_places=2, blank=True, null=True)
  total_price    = models.DecimalField(verbose_name='Суммарная стоимость товара',max_digits=10, decimal_places=2, blank=True, null=True)
  ordered        = models.BooleanField(verbose_name='Заказан?',default=False, blank=True, null=True)
  created        = models.DateTimeField(verbose_name='Дата создания',auto_now_add=True, auto_now=False, blank=True, null=True)
  updated        = models.DateTimeField(verbose_name='Дата обновления',auto_now_add=False, auto_now=True, blank=True, null=True)
  # objects        = OrderItemManager()
  def __str__(self):
    return f'{self.item.name}, {self.quantity}штука, {self.total_price}'
  def save(self, *args, **kwargs):
    price_per_item = self.item.price
    self.price_per_item = price_per_item
    self.total_price = self.quantity * price_per_item
    super().save(*args, **kwargs)
  def get_total_price(self, *args, **kwargs):
    return self.quantity * self.item.price
  class Meta: verbose_name = _('Товар в корзині'); verbose_name_plural = _('Товари в корзинах'); 







# Салфетки







# Заказы

ORDER_STATUS_CHOICES = (
	(_('Принят в обработку'), _('Принят в обработку')),
	(_('Выполняется'), _('Выполняется')),
	(_('Выполнен'), _('Выполнен')),
	# (_('Оплачен'), _('Оплачен')),
)


NAPKIN_STATUS_CHOICES = (
	(_('Принят в обработку'), _('Принят в обработку')),
	(_('Выполняется'), _('Выполняется')),
	(_('Выполнен'), _('Выполнен')),
	# (_('Оплачен'), _('Оплачен')),
)


class UserContact(models.Model):
  request_type = models.CharField(verbose_name='Сторінка',max_length=120)
  email        = models.EmailField()
  tel          = models.CharField(verbose_name='Номер телефона',max_length=20)
  text         = models.TextField(verbose_name='Сообщение',blank=True, null=True)
  class Meta: verbose_name="Заповнена форма"; verbose_name_plural="Заповнені форми"
  def __str__(self):
    try:
      return f'{self.request_type},{self.email}, {self.tel}, {self.text}'
    except:
      return f'{self.request_type},{self.email}, {self.tel}'


class Status(models.Model):
  name = models.CharField(max_length=24, blank=True, null=True, default=None)
  is_active = models.BooleanField(default=True)
  created = models.DateTimeField(auto_now_add=True, auto_now=False)
  updated = models.DateTimeField(auto_now_add=False, auto_now=True)
  def __str__(self):
    return "Статус %s" % self.name
  class Meta: verbose_name = _('Статус'); verbose_name_plural = _('Статусы'); 


class OrderItemManager(models.Manager):
  def all(self):
    return super(OrderItemManager, self).get_queryset().filter(ordered=False)


class OrderItem(models.Model):
  user           = models.ForeignKey(User, verbose_name=_('Пользователь'),on_delete=models.CASCADE, blank=True, null=True)
  sk             = models.CharField(max_length=120, verbose_name=_('Ключ сессии'),blank=True, null=True)
  item           = models.ForeignKey('core.Item', verbose_name=_('Товар'),on_delete=models.CASCADE, blank=True, null=True)
  order          = models.ForeignKey('core.Order', related_name='items', verbose_name=_('Заказ'),on_delete=models.CASCADE, blank=True, null=True)
  # size           = models.CharField(verbose_name=_('Размер'),max_length=20, blank=True, null=True)
  quantity       = models.IntegerField(verbose_name=_('Количество'),default=1)
  price_per_item = models.DecimalField(verbose_name=_('Цена за шт.'),max_digits=10, decimal_places=2, blank=True, null=True)
  total_price    = models.DecimalField(verbose_name=_('Суммарная стоимость товара'),max_digits=10, decimal_places=2, blank=True, null=True)
  ordered        = models.BooleanField(verbose_name=_('Заказан ли?'),default=False, blank=True, null=True)
  created        = models.DateTimeField(verbose_name=_('Дата создания'),auto_now_add=True, auto_now=False, blank=True, null=True)
  updated        = models.DateTimeField(verbose_name=_('Дата обновления'),auto_now_add=False, auto_now=True, blank=True, null=True)
  # objects        = OrderItemManager()
  def __str__(self):
    return f'{self.item.name}, {self.quantity}штука, {self.total_price}'
  def save(self, *args, **kwargs):
    price_per_item = self.item.price
    self.price_per_item = price_per_item
    self.total_price = self.quantity * price_per_item
    super().save(*args, **kwargs)
  class Meta: verbose_name = _('Товар в заказе'); verbose_name_plural = _('Товары в заказе'); 


class Order(models.Model):
  user        = models.ForeignKey(User, verbose_name=_('Пользователь'),on_delete=models.CASCADE, blank=True, null=True)
  sk          = models.CharField(verbose_name=_('Ключ сессии'),max_length=120)
  total_price = models.DecimalField(verbose_name=_('Сумма заказа'),max_digits=10, decimal_places=2, default=0)
  name        = models.CharField(verbose_name=_('Имя'),max_length=120, blank=True, null=True)
  email       = models.CharField(max_length=120, blank=True, null=True)
  phone       = models.CharField(verbose_name=_('Номер телефона'),max_length=120, blank=True, null=True)
  address     = models.CharField(verbose_name=_('Адрес'),max_length=120, blank=True, null=True)
  comments    = models.TextField(verbose_name=_('Коментарии'),blank=True, null=True, default=None)
  payment_opt = models.CharField(verbose_name=_("Спосіб оплати"), blank=True, null=True, max_length=120, help_text='manager - без предоплати. \n privat - з предоплатою')
  delivery_opt= models.CharField(verbose_name=_("Спосіб доставки"), blank=True, null=True, max_length=120)
  ordered     = models.BooleanField(verbose_name=_('Заказ завершений'), default=False)
  status      = models.CharField(verbose_name=_('Статус'), max_length=120, choices=ORDER_STATUS_CHOICES, blank=True, null=True) 
  created     = models.DateTimeField(verbose_name=_('Дата создания'),auto_now_add=True, auto_now=False, blank=True, null=True)
  updated     = models.DateTimeField(verbose_name=_('Дата обновления'),auto_now_add=False, auto_now=True, blank=True, null=True)
  class Meta: verbose_name = _('Заказ товаров'); verbose_name_plural = _('Заказы товаров'); 
  def __str__(self):
    return f'{self.phone}|{self.name}|{self.email}|{self.address}' 


class Napkin(models.Model):
  sk       = models.CharField(max_length=120, blank=True, null=True)
  final    = models.ImageField(verbose_name=("Финальное изображение"), blank=True, null=True)
  logo     = models.ImageField(blank=True, null=True)
  # template = models.ForeignKey('constructor.NapkinTemplate', verbose_name="шаблон", on_delete=models.CASCADE, blank=True, null=True)
  colour      = models.CharField(verbose_name='Коллір',     max_length=120, blank=True, null=True)
  form_factor = models.CharField(verbose_name='Форм-фактор', max_length=120, blank=True, null=True)
  embossing   = models.CharField(verbose_name='Тиснення',  max_length=120, blank=True, null=True)
  folding     = models.CharField(verbose_name='Розворот',    max_length=120, blank=True, null=True)

  name     = models.CharField(verbose_name='Имя',max_length=120, blank=True, null=True)
  email    = models.CharField(max_length=120, blank=True, null=True)
  phone    = models.CharField(verbose_name='Номер телефона',max_length=120, blank=True, null=True)
  address  = models.CharField(verbose_name='Адрес',max_length=120, blank=True, null=True)
  comments = models.TextField(verbose_name='Коментарии',blank=True, null=True, default=None)
  delivery_opt= models.CharField(verbose_name=_("Способ доставки"), blank=True, null=True, max_length=120)
  
  ordered  = models.BooleanField(default=False)
  status   = models.CharField(max_length=120, blank=True, null=True, choices=NAPKIN_STATUS_CHOICES) 
  created  = models.DateTimeField(verbose_name='Дата создания',auto_now_add=True, auto_now=False, blank=True, null=True)
  updated  = models.DateTimeField(verbose_name='Дата обновления',auto_now_add=False, auto_now=True, blank=True, null=True)
  class Meta:
    verbose_name="Заказ салфетки"; verbose_name_plural="Закази салфетки"; 
  def __str__(self):
    try:
      return f'sk:{self.sk}|\nlogo:{self.logo}|\ntemplate:{self.template}'
    except Exception as e:
      return f'{self.logo}'


class Payment(models.Model):
  status              = models.CharField(max_length=120, blank=True, null=True)
  ip                  = models.CharField(max_length=120, blank=True, null=True)
  amount              = models.CharField(max_length=120, blank=True, null=True)
  currency            = models.CharField(max_length=120, blank=True, null=True)
  order               = models.OneToOneField('core.Order', verbose_name='Заказ',on_delete=models.CASCADE, blank=True, null=True)
  sender_phone        = models.CharField(max_length=120, blank=True, null=True)
  sender_first_name   = models.CharField(max_length=120, blank=True, null=True)
  sender_last_name    = models.CharField(max_length=120, blank=True, null=True)
  sender_card_mask2   = models.CharField(max_length=120, blank=True, null=True)
  sender_card_bank    = models.CharField(max_length=120, blank=True, null=True)
  sender_card_type    = models.CharField(max_length=120, blank=True, null=True)
  sender_card_country = models.CharField(max_length=120, blank=True, null=True)
  timestamp           = models.DateTimeField(verbose_name='Время',auto_now_add=True, blank=True, null=True)

  def __str__(self):
    return f'{self.order}|{self.amount}|{self.currency}'
  class Meta: 
    app_label = 'order'
    verbose_name = 'Оплата'; verbose_name_plural = 'Оплата'; 








# Контент  













# class NapkinTemplate(models.Model):
#   image       = models.ImageField(blank=True, null=True)
#   colour      = models.ForeignKey('core.Colour',     blank=True, null=True, on_delete=models.CASCADE)
#   form_factor = models.ForeignKey('core.FormFactor', blank=True, null=True, on_delete=models.CASCADE)
#   embossing   = models.ForeignKey('core.Embossing',  blank=True, null=True, on_delete=models.CASCADE)
#   folding     = models.ForeignKey('core.Folding',    blank=True, null=True, on_delete=models.CASCADE)
#   default     = models.BooleanField(default=False)
#   def __str__(self): return f'{self.image}'


# class Colour(models.Model):
#   name = models.CharField(max_length=20, blank=True, null=True)
#   code = models.CharField(max_length=20, blank=True, null=True)
#   rgb  = models.CharField(max_length=20, blank=True, null=True)
#   def __str__(self):
#     return f'{self.name}|{self.code}|{self.rgb}'


# class FormFactor(models.Model):
#   name = models.CharField(max_length=20)
#   code = models.CharField(max_length=20, blank=True, null=True)
#   def __str__(self):
#     return f'{self.name}|{self.code}'


# class Embossing(models.Model):
#   name = models.CharField(max_length=20)
#   code = models.CharField(max_length=20, blank=True, null=True)
#   image= models.ImageField(blank=True, null=True)
#   def __str__(self):
#     return f'{self.name}|{self.code}'


# class Folding(models.Model):
#   name = models.CharField(max_length=20)
#   code = models.CharField(max_length=20, blank=True, null=True)
#   image= models.ImageField(blank=True, null=True)
#   def __str__(self):
#     return f'{self.name}'

