from django.shortcuts import render, redirect, reverse 
from .models import * 

def index(request):
  page, _ = Page.objects.get_or_create(code='index')
  return render(request, 'index.html', locals())


def about(request):
  return render(request, 'horeca.html', locals())


def thank_you(request):
  return render(request, 'thank_you.html', locals())

