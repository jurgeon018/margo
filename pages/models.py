from django.db import models 
from tinymce.models import HTMLField
from django.shortcuts import reverse


class Page(models.Model):
  code = models.CharField(max_length=30, blank=True, null=True, unique=True)
  meta_title = models.CharField(max_length=120, blank=True, null=True)
  meta_description = models.TextField(blank=True, null=True)

  class Meta:
    app_label = 'pages'
    verbose_name="Сторінка"
    verbose_name_plural="Сторінки"
    

  def __str__(self):
    return f'{self.code}'


class PageFeature(models.Model):
  page  = models.ForeignKey('Page', related_name="features", on_delete=models.CASCADE, blank=True, null=True)
  code  = models.CharField(max_length=120, unique=True, null=True, blank=True)
  name  = models.CharField(max_length=120, null=True, blank=True)
  value = models.TextField(max_length=120, null=True, blank=True)

  class Meta:
    app_label = 'pages'
    verbose_name="Характеристика сторінка"
    verbose_name_plural="Характеристики сторінок"

  def __str__(self):
    return f'{self.page.code}, {self.code}'


class Slider(models.Model):
  page = models.ForeignKey(to="Page", related_name="sliders", on_delete=models.CASCADE)
  image = models.ImageField()
  alt = models.CharField(max_length=120)
  text = models.TextField()

  class Meta:
    verbose_name="Слайдер"
    verbose_name_plural="Слайдери"
    
  def __str__(self):
    return f'{self.page.code}, {self.code}'


<<<<<<< HEAD

class Post(models.Model):
  title   = models.CharField(verbose_name=("Заголовок"),max_length=120, blank=True, null=True)
  description = models.TextField(blank=True, null=True)
  content = HTMLField(verbose_name=("Контент"), blank=True, null=True)  
  slug    = models.SlugField(verbose_name="Ссилка", blank=False, null=False, max_length=255, unique=True)
  image   = models.ImageField(verbose_name="Картинка", blank=True, null=True)
  updated = models.DateTimeField(verbose_name="Оновлено", auto_now_add=False, auto_now=True)
  created = models.DateTimeField(verbose_name="Створено", auto_now_add=True, auto_now=False)
  def __str__(self):
    return f'{self.title}'
  class Meta:
    app_label = 'content'
    verbose_name = 'Пост'; verbose_name_plural = 'Посты'
  def get_absolute_url(self):
      return reverse("post_detail", kwargs={"slug": self.slug})

=======
>>>>>>> cf4dc8b1376017a23266d2d63dfe19689278094a
