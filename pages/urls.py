from django.urls import path 
from .views import * 


urlpatterns = [
  path("",            index,     name='index'),
  path("about/",      about,     name='about'),
  path("thank_you/",  thank_you, name='thank_you'),
]
