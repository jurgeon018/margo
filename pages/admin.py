from django.contrib import admin
from core.admin import custom_admin
from pages.models import *

from core.admin import custom_admin


class PageFeatureInline(admin.TabularInline):
    model = PageFeature
    extra = 0


@admin.register(Page, site=custom_admin)
class PageAdmin(admin.ModelAdmin):
    inlines = [
        PageFeatureInline, 
    ]

@admin.register(PageFeature, site=custom_admin)
class PageFeatureAdmin(admin.ModelAdmin):
    pass


@admin.register(Slider, site=custom_admin)
class SliderAdmin(admin.ModelAdmin):
    pass



# custom_admin.register(Page)
# custom_admin.register(PageFeature)
# custom_admin.register(PageValue)




# class IndexPageAdmin(admin.ModelAdmin):
#     list_display_links = [
#         'id'
#     ]
#     list_display = [
#         'id',
#         'title',
#         'description',
#     ]
#     list_editable = [
#         'title',
#         'description',
#     ]
#     def has_add_permission(self, request):
#         count = IndexPage.objects.all().count()
#         if count == 0:
#           return True
#         return False 
