from modeltranslation.translator import (
    translator, TranslationOptions, register
)
from .models import *




@register(Page)
class PageTranslationOptions(TranslationOptions):
    fields = (
        'meta_title',
        'meta_description',
    )


@register(PageFeature)
class PageFeatureTranslationOptions(TranslationOptions):
    fields = (
        'value',
    )







