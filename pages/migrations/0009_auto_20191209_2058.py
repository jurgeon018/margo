# Generated by Django 3.0 on 2019-12-09 20:58

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0008_auto_20191209_2057'),
    ]

    operations = [
        migrations.RenameField(
            model_name='page',
            old_name='name',
            new_name='code',
        ),
    ]
