from django.contrib.sitemaps import Sitemap 
from blog.models import Post 



class StaticViewSitemap(Sitemap):
    def items(self):
        return [
            "index",
            "about",
        ]
    def location(self, item):
        return reverse(item)



