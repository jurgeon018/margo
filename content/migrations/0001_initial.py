<<<<<<< HEAD
# Generated by Django 2.2.8 on 2019-12-09 10:29

from django.db import migrations, models
import django.db.models.deletion
=======
# Generated by Django 3.0 on 2019-12-09 19:30

from django.db import migrations, models
>>>>>>> cf4dc8b1376017a23266d2d63dfe19689278094a
import tinymce.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Post',
<<<<<<< HEAD
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(blank=True, max_length=120, null=True, verbose_name='Заголовок')),
                ('description', models.TextField(blank=True, null=True)),
                ('content', tinymce.models.HTMLField(blank=True, null=True, verbose_name='Контент')),
                ('slug', models.SlugField(max_length=255, unique=True, verbose_name='Ссилка')),
                ('image', models.ImageField(blank=True, null=True, upload_to='', verbose_name='Картинка')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Оновлено')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Створено')),
            ],
            options={
                'verbose_name': 'Пост',
                'verbose_name_plural': 'Посты',
            },
        ),
        migrations.CreateModel(
            name='IndexSlider',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=120, verbose_name='Заголовок')),
                ('title_ru', models.CharField(max_length=120, null=True, verbose_name='Заголовок')),
                ('title_en', models.CharField(max_length=120, null=True, verbose_name='Заголовок')),
                ('title_uk', models.CharField(max_length=120, null=True, verbose_name='Заголовок')),
                ('content', models.TextField(verbose_name='Контент')),
                ('content_ru', models.TextField(null=True, verbose_name='Контент')),
                ('content_en', models.TextField(null=True, verbose_name='Контент')),
                ('content_uk', models.TextField(null=True, verbose_name='Контент')),
                ('image', models.ImageField(upload_to='', verbose_name='Зображення')),
                ('alt', models.CharField(blank=True, max_length=120, null=True)),
                ('link', models.URLField(blank=True, null=True)),
                ('button', models.BooleanField(default=True, verbose_name="Кнопка 'детальніше'")),
                ('page', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='sliders', to='pages.IndexPage', verbose_name='Сторінка')),
            ],
            options={
                'verbose_name': 'Слайдер на головній',
                'verbose_name_plural': 'Слайдери на головній',
            },
        ),
        migrations.CreateModel(
            name='HorecaSlider',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=120, verbose_name='Заголовок')),
                ('title_ru', models.CharField(max_length=120, null=True, verbose_name='Заголовок')),
                ('title_en', models.CharField(max_length=120, null=True, verbose_name='Заголовок')),
                ('title_uk', models.CharField(max_length=120, null=True, verbose_name='Заголовок')),
                ('content', models.TextField(verbose_name='Контент')),
                ('content_ru', models.TextField(null=True, verbose_name='Контент')),
                ('content_en', models.TextField(null=True, verbose_name='Контент')),
                ('content_uk', models.TextField(null=True, verbose_name='Контент')),
                ('image', models.ImageField(upload_to='', verbose_name='Зображення')),
                ('alt', models.CharField(blank=True, max_length=120, null=True)),
                ('link', models.URLField(blank=True, null=True)),
                ('button', models.BooleanField(default=True, verbose_name="Кнопка 'детальніше'")),
                ('page', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='sliders', to='pages.HorecaPage', verbose_name='Сторінка')),
            ],
            options={
                'verbose_name': 'Слайдер на хореці',
                'verbose_name_plural': 'Слайдери на хореці',
=======
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(blank=True, max_length=120, null=True, verbose_name='Заголовок')),
                ('description', models.TextField(blank=True, null=True, verbose_name='Опис')),
                ('meta_title', models.CharField(blank=True, max_length=120, null=True, verbose_name='Мета Заголовок')),
                ('meta_description', models.TextField(blank=True, null=True, verbose_name='Мета Опис')),
                ('content', tinymce.models.HTMLField(blank=True, null=True, verbose_name='Контент')),
                ('slug', models.SlugField(max_length=255, unique=True, verbose_name='Ссилка')),
                ('image', models.ImageField(blank=True, null=True, upload_to='', verbose_name='Картинка')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Оновлено')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Створено')),
            ],
            options={
                'verbose_name': 'Пост',
                'verbose_name_plural': 'Посты',
>>>>>>> cf4dc8b1376017a23266d2d63dfe19689278094a
            },
        ),
    ]
